# PINN4DE

Physics Informed Neural Networks

### Seminar Work for Industial Mathematics
pinn_seminar.pdf is the work presented for the seminar of 2021, in Numerics of Differential Equations. 

### Papers used
[![Paper](https://doi.org/10.1137/19M1274067)](https://epubs.siam.org/doi/pdf/10.1137/19M1274067)
[![Paper](https://doi.org/10.1016/j.jcp.2018.10.045)](https://arxiv.org/pdf/1711.10561.pdf)

@github
https://github.com/maziarraissi/PINN
